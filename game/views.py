from rest_framework import viewsets
import game.serializers as serializers
from django.contrib.auth.models import User
import game.models as models
import json
from django.http import HttpResponse
from django.views.generic.edit import FormView
from rest_framework.decorators import list_route
from rest_framework.response import Response


class UserViewSet(viewsets.ModelViewSet):
    """
    A viewset that provides the standard actions
    """
    queryset = User.objects.all()
    serializer_class = serializers.UserSerializer
    lookup_field = ('username')

    @list_route()
    def me(self, request):
        instance = request.user
        serializer = self.get_serializer(instance)
        return Response(serializer.data)


class GameViewSet(viewsets.ModelViewSet):
    """
    A viewset to CRUD GameSerializer
    """
    queryset = models.Game.objects.all()
    serializer_class = serializers.GameSerializer


class UserGameViewSet(viewsets.ModelViewSet):
    """
    A viewset to CRUD User Game objects
    """
    queryset = models.UserGame.objects.all()
    serializer_class = serializers.UserGameSerializer

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)

    def perform_update(self, serializer):
        serializer.save(owner=self.request.user)


class UserGameCreateViewSet(viewsets.ModelViewSet):
    """
    A viewset to CRUD User Game objects
    """
    queryset = models.UserGame.objects.all()
    serializer_class = serializers.UserGameCreateSerializer

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)

    def perform_update(self, serializer):
        serializer.save(owner=self.request.user)


# def get_games(request):
#     if request.is_ajax():
#         q = request.GET.get('term', '')
#         games = models.Game.objects.filter(name__icontains=q)
#         results = []
#         for gn in games:
#             game_json = {}
#             game_json = gn.name
#             results.append(game_json)
#             data = json.dumps(results)
#     else:
#         data = 'fail'
#         mimetype = 'application/json'
#     return HttpResponse(data, mimetype)
#

class AutoCompleteView(FormView):
    def get(self, request, *args, **kwargs):
        data = request.GET
        game = data.get("term")
        if game:
            games = models.Game.objects.filter(name__icontains=game)
        else:
            games = models.Game.objects.all()
        results = []
        for game in games:
            game_json = {}
            game_json['id'] = game.id
            game_json['label'] = game.name
            game_json['value'] = game.name
            results.append(game_json)

        data = json.dumps(results)
        mimetype = 'application/json'
        return HttpResponse(data, mimetype)
