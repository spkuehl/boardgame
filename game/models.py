from django.db import models
from django.contrib.auth.models import User


class Game(models.Model):
    name = models.CharField(max_length=35)
    year_published = models.PositiveSmallIntegerField(null=True, blank=True)

    # publisher
    # mechanic

    min_players = models.PositiveSmallIntegerField(null=True, blank=True)
    max_players = models.PositiveSmallIntegerField(null=True, blank=True)
    pref_players = models.PositiveSmallIntegerField(null=True, blank=True)

    min_time = models.FloatField(null=True, blank=True)
    max_time = models.FloatField(null=True, blank=True)
    ave_time = models.FloatField(null=True, blank=True)

    def __str__(self):
        return self.name


class UserGame(models.Model):
    game = models.ForeignKey(Game)
    owner = models.ForeignKey(User, related_name='games')
    date_added = models.DateField(auto_now_add=True)

    class Meta:
        unique_together = ('game', 'owner')

    def __unicode__(self):
        return '%d: %s' % (self.game, self.game.ave_time)

    def __str__(self):
        return self.game.name
