from rest_framework import serializers
from django.contrib import auth
from .models import Game, UserGame
from rest_framework.permissions import IsAuthenticated


class GameSerializer(serializers.ModelSerializer):
    permission_classes = (IsAuthenticated,)

    class Meta:
        model = Game
        fields = '__all__'


class UserGameSerializer(serializers.ModelSerializer):
    game = GameSerializer()
    permission_classes = (IsAuthenticated,)
    owner = serializers.ReadOnlyField(source='owner.username')

    class Meta:
        model = UserGame
        fields = ('owner', 'game', 'date_added')
        # read_only_fields = ('owner',)


class UserGameCreateSerializer(serializers.ModelSerializer):
    # game = GameSerializer()
    permission_classes = (IsAuthenticated,)
    owner = serializers.ReadOnlyField(source='owner.username')

    class Meta:
        model = UserGame
        fields = ('owner', 'game', 'date_added')


class UserSerializer(serializers.ModelSerializer):
    permission_classes = (IsAuthenticated,)
    # games = serializers.HyperlinkedRelatedField(
    #     many=True,
    #     read_only=True,
    #     view_name='game-detail'
    # )
    # games = serializers.PrimaryKeyRelatedField(many=True, read_only=True)
    games = UserGameSerializer(many=True)

    class Meta:
        model = auth.models.User
        fields = ('username', 'games')
        lookup_field = 'username'
        extra_kwargs = {
            'url': {'lookup_field': 'username'}
        }
