from django.test import TestCase
from .models import Game


class ModelTestCase(TestCase):
    """This class defines the test suite for the game model."""

    def setUp(self):
        """Define the test client and other test variables."""
        self.game_name = "Monopoly"
        self.game = Game(name=self.game_name)

    def test_model_can_create_a_game(self):
        """Test the game model can create a game."""
        old_count = Game.objects.count()
        self.game.save()
        new_count = Game.objects.count()
        self.assertNotEqual(old_count, new_count)
