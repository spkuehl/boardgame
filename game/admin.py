from django.contrib import admin
from .models import Game, UserGame
# Register your models here.


@admin.register(Game)
class GameAdmin(admin.ModelAdmin):
    list_display = ('name',)


@admin.register(UserGame)
class UserGameAdmin(admin.ModelAdmin):
    list_display = ('game', 'owner', 'date_added')
