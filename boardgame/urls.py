"""boardgame URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.conf.urls import url
from rest_framework import routers
import game.views as views
from django.views.generic import TemplateView
from django.contrib.auth.decorators import login_required
from rest_framework_jwt.views import obtain_jwt_token
from django.contrib.auth import views as auth_views
from django.conf.urls import include

# Wire up our API using automatic URL routing.
# Additionally, we include login URLs for the browsable API.


router = routers.DefaultRouter()
router.register(r'users', views.UserViewSet)
router.register(r'games', views.GameViewSet)
router.register(r'usergames', views.UserGameViewSet)
router.register(r'createusergames', views.UserGameCreateViewSet)

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^$', login_required(TemplateView.as_view(
        template_name="home.html")), name='home'),
    url(r'^api/get_games/$', views.AutoCompleteView.as_view(),
        name='get_games'),
    url(r'^api-token-auth/', obtain_jwt_token),
    url(r'^accounts/login/$', auth_views.login,
        {'template_name': 'registration/login.html'}, name='login'),
    url(r'^logout/$', auth_views.logout,
        {'next_page': 'login'}, name='logout'),
    url(r'^api/v1/', include(router.urls)),
    ]
