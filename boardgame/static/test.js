$(document).ready(function(){
    function getCookie(name) {
        var cookieValue = null;
        if (document.cookie && document.cookie != '') {
            var cookies = document.cookie.split(';');
            for (var i = 0; i < cookies.length; i++) {
                var cookie = jQuery.trim(cookies[i]);
                // Does this cookie string begin with the name we want?
                if (cookie.substring(0, name.length + 1) == (name + '=')) {
                    cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                    break;
                }
            }
        }
        return cookieValue;
    }
    function loadTable() {
        $.getJSON( "http://127.0.0.1:8000/api/v1/users/me", function( data ) {
          var items = [];
          console.log(data);
          $("#game-list tbody tr").remove();
          $.each( data.games, function( id, game ) {
            $( "<tr/>", {
              html: "<td>"+(id+1)+"</td><td>"+game.game.name+"</td><td>"+game.game.year_published+"</td>"
            }).appendTo( "tbody" );
          });
        });
    }

    loadTable();

    var selected_game;

    $(function() {
      $("#new-game").autocomplete({
        source: "/api/get_games/",
        minLength: 2,
        select: function(event, ui) {
            selected_game = ui.item.id;
            console.log(ui);
        }
      });
    });
    $("#new-game-post").click(function(){
        // Assemble form data
        // Post data to create new game
        // Show errors
        // Reload list if successful
        $.ajax({
            url: "/api/v1/createusergames/",
            type: "POST",
            data: JSON.stringify({game: selected_game}),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            headers: {
                'X-CSRFTOKEN': getCookie("csrftoken")
            },
            success: function(data){
                loadTable();
                console.log(data);
            }
        });
    //   $.getJSON( "http://127.0.0.1:8000/users/me/", function( data ) {
    //     console.log(data);
    //   });
    });

});
//
//
// <tr>
//   <th scope="row">1</th>
//   <td>Dixit</td>ave_time
//   <td>Storytelling</td>
//   <td>1:00</td>
//   <td>3</td>
//   <td>12</td>
// </tr>
