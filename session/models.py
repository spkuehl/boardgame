from django.db import models
from model_utils import Choices
from model_utils.fields import StatusField
from game.models import Game
from django.contrib.auth.models import User


class GameSession(models.Model):
    game = models.ForeignKey(Game)
    start = models.DateTimeField()
    end = models.DateTimeField(blank=True, null=True)


class PlayerSession(models.Model):
    user = models.ForeignKey(User)
    game_session = models.ForeignKey(GameSession)
    STATUS = Choices('Win', 'Loss', 'Draw', 'No Contest')
    result = StatusField(blank=True, null=True)
    placement = models.PositiveSmallIntegerField(blank=True, null=True)
